# ingress-nginx

This repository has been fully included in the toolforge-deploy repository:
https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy

Any new changes should go there instead.
